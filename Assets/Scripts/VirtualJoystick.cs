using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VirtualJoystick : MonoBehaviour , IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    private Image backgroundImg;
    private Image buttonImg;
    private Vector3 inputVector;
    public bool triggered = false;

    void Awake()
    {
        backgroundImg = GetComponent<Image>();
        buttonImg = transform.GetChild(0).GetComponent<Image>();
    }

    public virtual void OnDrag(PointerEventData eventData)
    {
        Vector2 inputPos;
        if(RectTransformUtility.ScreenPointToLocalPointInRectangle(backgroundImg.rectTransform , eventData.position , eventData.pressEventCamera, out inputPos))
            {
                inputPos.x = (inputPos.x / backgroundImg.rectTransform.sizeDelta.x);
                inputPos.y = (inputPos.y / backgroundImg.rectTransform.sizeDelta.y);

                inputVector = new Vector3(inputPos.x*2 +1 , 0, inputPos.y*2 -1);

                inputVector = (inputVector.magnitude > 1.0f)? inputVector.normalized : inputVector;

                buttonImg.rectTransform.anchoredPosition =
                    new Vector3(inputVector.x * (backgroundImg.rectTransform.sizeDelta.x / 3),
                            inputVector.z * (backgroundImg.rectTransform.sizeDelta.y / 3));

                Debug.Log(inputVector);
            }
    }

    public virtual void OnPointerDown(PointerEventData eventData)
    {
        triggered = true;
        OnDrag(eventData);
    }

    public virtual void OnPointerUp(PointerEventData eventData)
    {
        triggered= false;
        inputVector = Vector3.zero;
        buttonImg.rectTransform.anchoredPosition = Vector3.zero;
    }

    public float JoystickHorizontal() 
    {
        return inputVector.x;
    }

    public float JoystickVertical()
    {
        return inputVector.z;
    }

    void Start()
    {
        
    }


    void Update()
    {
        
    }
}
