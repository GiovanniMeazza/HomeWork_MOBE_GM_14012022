using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScorePanel : MonoBehaviour
{
    private Transform _scoreTxt;
    public int scoreToWin;
    private int currentScore = 0;
    private int cachedScore;
    private Ball_Script ball;
    private GameObject target;
    private void Awake()
    {
        _scoreTxt = transform.GetChild(1);
        cachedScore = currentScore;
    }

    // Start is called before the first frame update
    void Start()
    {
        _scoreTxt.GetComponent<Text>().text = currentScore.ToString();
        ball = GameObject.Find("Ball").GetComponent<Ball_Script>();
        ball.gainPoint += Ball_gainPoint;
        target = GameObject.Find("Target");
        target.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        CheckScore();
    }

    private void CheckScore()
    {
        if (cachedScore != currentScore)
        {
            cachedScore = currentScore;
            _scoreTxt.GetComponent<Text>().text = cachedScore.ToString();
        }
    }

    private void Ball_gainPoint()
    {
        currentScore++;
        if (currentScore == scoreToWin)
        {
            target.SetActive(true);
        }
    }
}
