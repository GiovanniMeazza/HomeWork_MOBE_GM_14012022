using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ball_Script : MonoBehaviour
{
    private float touchSpeed = 5f;
    [SerializeField] private Camera _cam;
    [SerializeField] private Rigidbody _rig;
    public VirtualJoystick joystick;
    public event Action gainPoint;

    private void Awake()
    {
        _cam = Camera.main;
        _rig = GetComponent<Rigidbody>();
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (joystick.GetComponent<VirtualJoystick>().triggered)
            JoystickMovement();
        else if (Input.touchCount > 0)
            TouchMovement();
        else
            AccelerMovement();
    }

    void TouchMovement()
    {
        Touch touch = Input.GetTouch(0);
        if (touch.phase == TouchPhase.Stationary)
        {
            Vector3 pointedPos = _cam.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, _cam.nearClipPlane));
            // _rig.AddForce(((pointedPos - transform.position) * touchSpeed).normalized, ForceMode.Impulse);
            pointedPos.y -= pointedPos.y;
            _rig.AddForce((pointedPos - transform.position).normalized * touchSpeed , ForceMode.Acceleration);
            Debug.Log((pointedPos - transform.position).normalized);
            //Debug.DrawRay(transform.position, new Vector3(pointedPos.x, 0, pointedPos.z), Color.red);
        }
    }

    void AccelerMovement() 
    {
        Vector3 movement = new Vector3(Input.acceleration.x, 0.0f, Input.acceleration.y);
        _rig.AddForce(movement * touchSpeed, ForceMode.Acceleration);
    }

    void JoystickMovement()
    {
        Vector3 movement = new Vector3(joystick.JoystickHorizontal(), 0.0f, joystick.JoystickVertical());
        _rig.AddForce(movement * touchSpeed, ForceMode.Acceleration);
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Item"))
        {
            collision.transform.parent = transform;
            gainPoint?.Invoke();
        }

        if (collision.gameObject.CompareTag("Target"))
        {
            SceneManager.LoadScene(0);
        }
    }
}
