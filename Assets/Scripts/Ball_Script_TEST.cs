using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball_Script_TEST : MonoBehaviour
{
    private float keySpeed = 0.3f;
    private float mouseSpeed = 2f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.anyKey)
            this.gameObject.GetComponent<Rigidbody>().AddForce(Input.GetAxis("Horizontal")* keySpeed, 0 , Input.GetAxis("Vertical") * keySpeed, ForceMode.Impulse);
        else           
            this.gameObject.GetComponent<Rigidbody>().AddForce(Input.GetAxis("Mouse X") * mouseSpeed, 0, Input.GetAxis("Mouse Y") * mouseSpeed, ForceMode.Impulse);

    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Item"))
            collision.transform.parent = transform;
    }
}
